jQuery.noConflict();

jQuery(document).ready(function ($) {
  var i;
  for (i in Drupal.settings.booklet) {
    var booklet = Drupal.settings.booklet[i];
    booklet.settings.speed = parseFloat(booklet.settings.speed);
    booklet.settings.startingPage = parseFloat(booklet.settings.startingPage);
    booklet.settings.closed = Boolean(booklet.settings.closed);
    booklet.settings.closed = Boolean(booklet.settings.closed);
    booklet.settings.covers = Boolean(booklet.settings.covers);
    booklet.settings.autoCenter = Boolean(booklet.settings.autoCenter);
    booklet.settings.pagePadding = parseFloat(booklet.settings.pagePadding);
    booklet.settings.pageNumbers = Boolean(booklet.settings.pageNumbers);
    booklet.settings.pageBorder = parseFloat(booklet.settings.pageBorder);
    booklet.settings.manual = Boolean(booklet.settings.manual);
    booklet.settings.hovers = Boolean(booklet.settings.hovers);
    booklet.settings.hoverWidth = parseFloat(booklet.settings.hoverWidth);
    booklet.settings.hoverSpeed = parseFloat(booklet.settings.hoverSpeed);
    booklet.settings.hoverTreshold = parseFloat(booklet.settings.hoverTreshold);
    booklet.settings.hoverClick = Boolean(booklet.settings.hoverClick);
    booklet.settings.overlays = Boolean(booklet.settings.overlays);
    booklet.settings.tabs = Boolean(booklet.settings.tabs);
    booklet.settings.arrows = Boolean(booklet.settings.arrows);
    booklet.settings.arrowsHide = Boolean(booklet.settings.arrowsHide);
    booklet.settings.hash = Boolean(booklet.settings.hash);
    booklet.settings.auto = Boolean(booklet.settings.auto);
    booklet.settings.delay = parseFloat(booklet.settings.delay);
    booklet.settings.pageSelector = Boolean(booklet.settings.pageSelector);
    booklet.settings.chapterSelector = Boolean(booklet.settings.chapterSelector);
    booklet.settings.shadows = Boolean(booklet.settings.shadows);
    booklet.settings.shadowTopFwdWidth = parseFloat(booklet.settings.shadowTopFwdWidth);
    booklet.settings.shadowTopBackWidth = parseFloat(booklet.settings.shadowTopBackWidth);
    booklet.settings.shadowBtmWidth = parseFloat(booklet.settings.shadowBtmWidth);
    $(booklet.selector + ' .field-items').booklet(booklet.settings);
    if (Boolean(Drupal.settings.booklet[i].thumbnails.status)) {
      if (booklet.thumbnails.placement == 'before') {
        $(booklet.selector + ' .field-items').before('<div class="thumbnails"></div>');
      }
      if (booklet.thumbnails.placement == 'after') {
        $(booklet.selector + ' .field-items').after('<div class="thumbnails"></div>');
      }
      $(booklet.selector + ' a[data-digitalpublications-thumbail]').each(function(index) {
        var thumbail = $(this).attr('data-digitalpublications-thumbail');
        if (index % 2 != 0) {
          index = index + 1;
        }
        $(booklet.selector + ' .thumbnails').append('<span><a href="#/page/' + (index + 1) + '"><img src="' + thumbail + '">');
      });
    }
    $('.booklet-colorbox').colorbox({
      maxWidth: "100%",
    });
  }
});
