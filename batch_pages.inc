<?php
/**
 * @file
 * Batch functions to process publication pages.
 */

use XPDF\PdfToText;

/**
 * Batch Operation Callback.
 */
function digital_publications_batch_pages_process($i, $entity, $type, $field, $pdf_file, $format, &$context) {
  if (isset($context['results']['entity'])) {
    $entity = $context['results']['entity'];
  }

  $id = entity_extract_ids($type, $entity);
  $id = reset($id);
  $pagenum = $i + 1;

  if ($field['widget']['type'] == 'page_content' || $field['widget']['type'] == 'page_image_content') {
    $pdftotext_bin = variable_get('digital_publications_pdftotext_bin', '/usr/bin/pdftotext');
    $pdf_to_ext = PdfToText::create(array('pdftotext.binaries' => $pdftotext_bin));
    $text = $pdf_to_ext->getText(drupal_realpath($pdf_file->uri), $pagenum, $pagenum);
    $entity->{$field['field_name']}[LANGUAGE_NONE][$i]['content'] = $text;
    $entity->{$field['field_name']}[LANGUAGE_NONE][$i]['format'] = $format;
  }

  if ($field['widget']['type'] == 'page_image' || $field['widget']['type'] == 'page_image_content') {
    $directory = file_default_scheme() . '://' . $field['settings']['image_settings']['file_directory'];
    $file = file_load($entity->{$field['field_name']}[LANGUAGE_NONE][$i]['image']);

    if ($file !== FALSE) {
      $file = digital_publications_exporte_pdf($pdf_file, $file, $field, $directory, $i, $id, $pagenum, $entity);
    }
    else {
      $file = new stdClass();
      $file->uid = $entity->uid;
      $file = digital_publications_exporte_pdf($pdf_file, $file, $field, $directory, $i, $id, $pagenum, $entity);
      $file_by_uri = file_load_multiple(array(), array('uri' => $file->uri));
      $file_by_uri = reset($file_by_uri);
      if ($file_by_uri) {
        $file = $file_by_uri;
      }
      else {
        $file = file_save($file);
        file_usage_add($file, 'digital_publications', $type, $id);
      }
    }

    $entity->{$field['field_name']}[LANGUAGE_NONE][$i]['image'] = $file->fid;
  }

  $context['results']['pages'][] = $pagenum;
  $context['results']['entity'] = $entity;
  $context['results']['type'] = $type;
  $context['results']['id'] = $id;
}

/**
 * Batch 'finished' callback.
 */
function digital_publications_batch_pages_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    field_attach_update($results['type'], $results['entity']);
    entity_get_controller($results['type'])->resetCache(array($results['id']));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ));
    drupal_set_message($message, 'error');
  }
}
/**
 * Export a specific pdf page to an image file and returns a Drupal file object.
 */
function digital_publications_exporte_pdf($pdf_file, $file, $field, $directory, $i, $id, $pagenum, $entity) {
  $file->filename = $entity->type . "-" . $id . "-page-" . $pagenum . "." . $field['settings']['image_format'];
  $file->uri = $directory . '/' . $file->filename;
  $filemime = ($field['settings']['image_format'] == 'jpg') ? 'jpeg' : $field['settings']['image_format'];
  $file->filemime = 'image/' . $filemime;
  $file->status = 0;

  $img = new imagick();
  $img->setResolution($field['settings']['resolution'], $field['settings']['resolution']);
  $img->readImage(drupal_realpath($pdf_file->uri) . "[$i]");
  $img->setImageFormat($field['settings']['image_format']);

  if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
    $img->writeImages(drupal_realpath($file->uri), FALSE);
  }
  return $file;
}
