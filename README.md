Provide Field to help build Digital Publications Feature,
like [ISSUU](http://issuu.com) service.
The display is formatted using the
[Booklet - jQuery Plugin](http://builtbywill.com/code/booklet/).

Features
--------

  * 3 types of widgets: "Image and Content", "Only Image" and "Only Content"
  * Images can use image styles
  * Content can use text formats
  * Cover formatter with image styles
  * Automatic publication generation from PDF with File Field


Requirements
------------

  * Image (in Drupal core)
  * [jQuery Update](https://www.drupal.org/project/jquery_update)
  * [Libraries API](https://www.drupal.org/project/libraries)
