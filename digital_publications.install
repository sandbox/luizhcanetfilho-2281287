<?php
/**
 * @file
 * Module installation functions for Digital Publications.
 */

/**
 * Implements hook_requirements().
 */
function digital_publications_requirements($phase) {

  $t = get_t();
  $requirements = array();

  if ($phase == 'runtime') {
    if (!module_exists('composer_manager')) {
      $requirements['digital_publications_composer'] = array(
        'title' => $t('Digital Publications: PHP Libraries'),
        'value' => 'Note using Composer Manager Module',
        'description' => $t(
            'To use the automatic page generation from PDF file field, you have
            to run "composer install" (See <a href="https://getcomposer.org/doc/00-intro.md#downloading-the-composer-executable">Composer</a>)
            from the module directory (@module_dir) or using the <a href="https://www.drupal.org/project/composer_manager">
            Composer Manager</a> module.',
            array(
              '@module_dir' => drupal_get_path('module', 'digital_publications'),
            )
        ),
        'severity' => REQUIREMENT_INFO,
      );
    }

    $booklet = libraries_detect('booklet');

    if (!$booklet['installed']) {
      $requirements['digital_publications_booklet'] = array(
        'title' => $t('Digital Publications: Booklet'),
        'value' => $t('Booklet not detected!'),
        'description' => $t(
            'To use the <a href="@vendor_url">Booklet</a>
            formatter for Digital Publications Field, you have to
            <a href="@download_url">download</a>
            and extract the Booklet jQueyr Plugin into the libraries folder as
            described in the <a href="https://www.drupal.org/project/libraries">Libraries
            API module page</a>.',
            array(
              '@vendor_url' => $booklet['vendor url'],
              '@download_url' => $booklet['download url'],
            )
        ),
        'severity' => REQUIREMENT_WARNING,
      );
    }
    else {
      $requirements['digital_publications_booklet'] = array(
        'title' => $t('Digital Publications: Booklet'),
        'value' => $t('Version @version intalled.', array('@version' => $booklet['version'])),
        'severity' => REQUIREMENT_INFO,
      );
    }

    $jquery_version = variable_get('jquery_update_jquery_version', '1.10');

    if (version_compare($jquery_version, '1.7', '<')) {
      $requirements['digital_publications_jquery'] = array(
        'title' => $t('Digital Publications: jQuery'),
        'value' => $t('Version: @version', array('@version' => $jquery_version)),
        'description' => $t(
            'To use the <a href="@vendor_url">Booklet</a>
            formatter for Digital Publications Field, you have to choose version
            1.7 or greater of jQuery in jQuery Update
            <a href="@jquery_update">configuration</a> page.',
            array(
              '@vendor_url' => $booklet['vendor url'],
              '@jquery_update' => url('admin/config/development/jquery_update'),
            )
        ),
        'severity' => REQUIREMENT_WARNING,
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_field_schema().
 */
function digital_publications_field_schema($field) {
  return array(
    'columns' => array(
      'image' => array(
        'description' => 'Image information of the Page',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'content' => array(
        'type' => 'text',
        'size' => 'big',
      ),
      'format' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
  );
}
